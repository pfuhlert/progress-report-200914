% !TEX program = xelatex

\documentclass[aspectratio=1610, xcolor={dvipsnames}]{beamer}

\usepackage{amsmath,amsfonts,amsthm,bm}
\usepackage{animate}
\usepackage[backend=biber, style=apa]{biblatex}
\addbibresource{bib/library.bib}
\usepackage{booktabs}
\usepackage[english]{babel}
\usepackage{caption}
\usepackage{csquotes}
%fonts
\usepackage{fontawesome}

\usepackage[sfdefault]{Fira Sans}
\usepackage[mathrm=sym]{unicode-math}
\usefonttheme{professionalfonts}
\setmathfont{Fira Math}
\setmathfont{TeX Gyre DejaVu Math}[range={\vdots,\ddots}]
%----
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{romannum}
\usepackage{siunitx}
\usepackage{tikz}
\usepackage{tikzscale}
\usepackage{standalone}

% Experimental

% ----------------------

\newcommand{\themename}{\textbf{\textsc{metropolis}}\xspace}

\usetikzlibrary{arrows.meta, fit, calc, matrix, positioning, chains, decorations.pathreplacing}
\usetheme{metropolis}
\title{Survival Analysis with Neural Networks on Electronic Health Records}
\date{14.09.2020}
\author{Patrick Fuhlert}
\institute{Institute of Medical Systems Biology}

\metroset{subsectionpage=progressbar, numbering=fraction, titleformat=smallcaps}

\makeatletter

\input{metropolis_extensions.tex}

\begin{document}

  \maketitle

  \begin{frame}[allowframebreaks]{Content}
    \setbeamertemplate{section in toc}[sections numbered]
    \tableofcontents[hideallsubsections]
  \end{frame}  

  \section{Introduction}
    
  \subsection{Prostate Cancer}

  \begin{frame}{Prostate Cancer (PCa)}

    \begin{columns}
    \begin{column}{0.6\textwidth}
    
    Worldwide statistics\footnotemark:
    \begin{itemize}
      \item second most common type of cancer in men
      \item fifth-leading cause of cancer-related death in men
      \item more than 1 million new cases per year
      \item up to 370,000 deaths per year
      \item Common treatment: Radical Prostatectomy
      \item Common side-effects of prostatectomy: urinary incontinence, erectile dysfunction
    \end{itemize}
    \end{column}
    \begin{column}{0.4\textwidth}
     \begin{figure}
         \centering
         \includegraphics[width=\textwidth]{img/pros_cancer.png}
         {\footnotesize
         [Cancer Research UK]}
     \end{figure}
    
    \end{column}
    \end{columns}
    \footnotetext{\tiny{https://www.wcrf.org/dietandcancer/cancer-trends/prostate-cancer-statistics}}
  \end{frame}

  \begin{frame}{Motivation}
    \begin{itemize}
      \item Direct Access to Martini-Clinic (UKE) data
      \begin{itemize}
        \item Prostate Cancer Clinic
        \item `With around  2400 prostate cancer operations per year [\ldots ] biggest Prostate Cancer Center [\ldots ] worldwide'\footnote{martini-klinik.de/en/for-patients/}
        \item Focus on (robot assisted) radical prostatectomy
      \end{itemize}
      \item We want to
      \begin{itemize}
        \item Find disease/medication/treatment \textbf{correlations}
        \item Predict risks and \textbf{future progression} for \textbf{clinical decision making}
        \item Find patient subgroups with varying predictions (stratification)
        \item Gain Results that are interpretable, avoid `\textbf{black box}' problem
      \end{itemize}
    \end{itemize}
  \end{frame}

    \begin{frame}{Patient Lifecycle}
      \begin{figure}[]
        \centering
        \includegraphics[width=.75\textwidth]{img/pca-lifecycle.png}
        \caption{\scriptsize{Simplified PCa Patient Lifecycle. AS=Active Surveillance, RT=Radiotherapy, RP=Radical Prostatectomy}}
      \end{figure}
      \begin{enumerate}
        \pause{}
        \item Is a biopsy necessary?
        \item What primary treatment should be applied?
        \item How long does a patient remain progression free with specific treatment?
        \item Is additional treatment after RP necessary?
      \end{enumerate}
    \end{frame}

    \section{Survival Analysis}

    \begin{frame}{Overview}
      Survival analysis tries to find the expected duration of time until one or more events happen.
      \hspace*\fill{\small---~\cite{MillerJr2011}}
      \vspace{1em}
      
      \begin{description}[leftmargin=1em, labelwidth=2em]
        \item[Medical Survival] How long until patient dies?
        \item[Reliability Theory] How long until something breaks?
        \item[Economics] How long until a customer withdraws?
      \end{description}
    \end{frame}

    \begin{frame}{Censoring}
      
      \begin{itemize}
        \item Typical for medical studies, not all included patients participate a study from start to finish
        \item If they only participate in parts of a study, their data is considered \alert{censored}
        \item We mostly deal with right-censored data (all patients joined at a certain time, but not all completed)
      \end{itemize}
    \end{frame}    

    \subsection{Classic Approach}

    \begin{frame}{Survival Curve}
      \begin{figure}[]
        \centering
        \includegraphics[height=.5\textheight]{img/survival-plot-sample-single.png}
        \caption{Survival Curve of non-smokers starting at age 50 (\cite{Doll})}
      \end{figure}

      Try to find predicted curve \(\hat{S}(t)\) that approximates real survival curve \(S(t)\)
    \end{frame}
  
    \begin{frame}{Kaplan-Meier Curves I}

      \begin{minipage}{.25\textwidth}
        \begin{table}[]
          \begin{tabular}{rl}
            & \(t_i\) \\
            \midrule
            A & 1 \\
            B & 2 \\
            C & 3 \\
            D & 4 \\
            E & 5 \\
          \end{tabular} \\
        \end{table} 
        \textbf{Table 1:} Toy survival times               
      \end{minipage}\begin{minipage}{.74\textwidth}
        \pause{}
        \begin{figure}
          \scalebox{0.5}{\input{pgf/km.pgf}}
          \caption{Kaplan-Meier Estimate}
        \end{figure}
      \end{minipage}

      Kaplan-Meier approach:\quad \(\hat{S}(t) = \displaystyle \prod_{t_i < t}(1 - \frac{d_i}{n_i})\) \\
      \vspace{.5em}
      \scriptsize{where \(n_i\) is number of people at risk and \(d_i\) = number of death events at time \(t_i\)}
      
    \end{frame}
    
    \begin{frame}{Stratification}
      \begin{figure}[]
        \centering
        \includegraphics[height=.65\textheight]{img/survival-plot-sample.png}
        \caption{Survival Curve of (non-) smokers starting at age 50 (\cite{Doll})}
      \end{figure}

    \end{frame}    

    \begin{frame}{Stratification II}
      \begin{itemize}
        \item Naive approach: Calculate Survival Group for subpopulations
        \item What about the influence of covariates?
        \item Default approach in medical statistics: Cox Proportional Hazards Model
      \end{itemize}

    \end{frame}     

    \begin{frame}{Cox Proportional Hazards Model I}
      \begin{itemize}
        \item Consider \(h(t) = 1 - S(t)\) to be called the hazard function
        \item Split hazard function in base hazard and influence of covariates
        \[h(t) = h_0(t) \cdot s\]
        \item \(h_0(t)\) is called the \alert{baseline hazard}. It corresponds to the hazard if all covariates are \(0\)
        \item Proportional Hazard Assumption:
        \item[] All individuals have the \alert{same hazard function}, but a \alert{unique scaling factor}
        
        
      \end{itemize}
    \end{frame}

    \begin{frame}{Cox Proportional Hazards Model II}
      \begin{itemize}
        \item Typically assumed that the hazard responds exponentially:
        \[h(t) = h_0(t) \cdot \underbrace{\exp(b_1 x_1 + b_2x_2 + \cdots + b_p x_p)}_{\text{Independent of } t}\]
        \hspace*\fill{\small---~\cite{Cox1972}}
        \item coefficients \(b_p\) measure the \alert{impact of covariates}
        \item if \(b_p > 0\) the covariate increases the hazard and thus decreases the length of survival
        \item Obtain \(b_p\) by Multivariate Linear Regression (independent of t)
      \end{itemize}
    \end{frame}

    \begin{frame}{Cox Proportional Hazards Model --- Example}
      \begin{figure}
        \includegraphics[width=.55\textwidth]{img/cox-survival-plot-example.png}
        \caption{Exemplary Survival Plot using Cox Model\footnote{\href{http://www.sthda.com/english/wiki/cox-proportional-hazards-model}{sthda.com/english/wiki/cox-proportional-hazards-model}}}
      \end{figure}
    \end{frame}
  
    \subsection{With Neural Networks}

    \begin{frame}{Neural Networks in Survival Analysis}
      Main Architectures:
      \begin{itemize}
        \item Combine with Cox PH Function (still uses cox assumption)
        \item Predict fixed points in the future (discrete time)
      \end{itemize}
    \end{frame}

    \begin{frame}{Neural Networks with Continuous Output}
      \begin{figure}[]
        \fbox{\includestandalone[height=.6\textheight]{tikz/continuous-survival-nn}}
        \caption{Hybrid Model with Continuous Output}
      \end{figure}
      \begin{itemize}
        \item \(O_1\) is used as exponent in Hazard Function \(h(x, t)\)
        \item Linear Combination of input vector corresponds to classical Cox PH
      \end{itemize}      
    \end{frame}

    \begin{frame}{Survival Prediction with Neural Networks II}
      \begin{figure}[]
        \centering
        \fbox{\includestandalone[width=.4\textwidth]{tikz/discrete-nn}}
        \caption{Neural Network with Discrete Output}
      \end{figure}
      
      \begin{itemize}
        \item \(O_1, \dots, O_m\) predict survival at certain times \(t_1, \dots, t_m\)
      \end{itemize}
    \end{frame}   

    \begin{frame}{Sample Discrete NN for Survival Prediction}
      \begin{figure}[]
        \centering
        \includegraphics[height=.8\textheight]{img/nn-example-discrete-survival.png}
        \caption{Sample NN with Discrete Output (\cite{Gensheimer2019})}
      \end{figure}
    \end{frame}  

    \subsection{Performance Metrics}

    \begin{frame}{Evaluation of Models}
      How to Evaluate?
      \begin{itemize}
        \item Censored Data in Ground truth
        \item Option 1: Only keep uncensored observations 
        \item Option 2: Use all the information that is provided
      \end{itemize}
    \end{frame}

    \begin{frame}{Concordance Index}
      \begin{itemize}
        \item Generalization of the area under the ROC curve for censored data
        \item Fraction of correct order of predictions \(\hat{t}_i\) vs outcome \(t_i\): 
        \[\text{c-index = } \displaystyle \frac{\sum_{i, j} 1_{t_j < t_i} \cdot 1_{\hat{t}_j < \hat{t}_i} \cdot \delta_i}{\sum_{i, j} 1_{t_j < t_i} \cdot \delta_i}\]
        \item Leave out a pair if the concordance is unknown due to censoring (\(\delta_i = 1\))
        \item Perfect score is \(1\), random prediction is \(\approx 0.5\)
      \end{itemize}
    \end{frame}

    \begin{frame}{Concordance Index --- Example}

      \begin{minipage}{0.49\textwidth}
        \begin{table}[]
          \begin{tabular}{rll}
            & \(t_i\) & \(\hat{t}_i\) \\
            \midrule
            A & 1 & 1  \\
            B & 2 & 2  \\
            C & 3 & 3  \\
            D & 4 & 4  \\
            E & 5 & 5 \\
          \end{tabular} \\
          \pause{}
        \end{table}
        \centering
        c-index = \(1\)
      \end{minipage}
      \pause{}
      \begin{minipage}{0.49\textwidth}
        \begin{table}[]
        \begin{tabular}{rll}
          & \(t_i\) & \(\hat{t}_i\) \\
          \midrule
          A & 1 & 2  \\
          B & 2 & 3  \\
          C & 3 & 5  \\
          D & 4 & 8  \\
          E & 5 & 14 \\
        \end{tabular} \\
      \end{table}
      \centering
      c-index = \(1\)      
      \end{minipage}
      \pause{}
      \begin{itemize}
        \item[]
        \item[]
        \item[] Only \alert{order} of predictions is important, not actual values!
      \end{itemize}
    \end{frame}    

    \begin{frame}{Concordance Index --- Example II}
      \begin{table}[]
          \centering
          \begin{tabular}{rlll}
            & \(t_i\) & \(\hat{t}_i\) & \(\delta_i\) \\
            \midrule
            A & 1 & 1 & 1 \\
            B & 2 & 2 & 1 \\
            C & 3 & 3 & 0 \\
            D & 4 & \alert{5} & 1 \\
            E & 5 & \alert{4} & 1 \\
          \end{tabular} \\
      \end{table}
      \vspace{1cm}
      \[\text{c-index = }\frac{|\{(B, A), (C, A), (C, B), (D, A), (D, B), (E, A), (E, B)\}|}{10 - |\{(D, C), (E, C)\}|} = 0.875\]      
    \end{frame}      

    \begin{frame}{Brier Score}
      \begin{itemize}
        \item Consider \(p_i = \hat{S}(x_i)\) as the predicted survival and \(o_i\) the observed survival of an individual at a specific time \(t\)
        \item[] \[BS = \frac{1}{N} \displaystyle \sum_{t=1}^{N} {(p_i - o_i)}^2\]
        \hspace*\fill{\small---~\cite{brier1951verification}}
        \item Mean squared error between prediction \(p_i\) and outcome \(o_i\)
        \item Loss Function --- Lower is better
        \item Ranges from \(0\) (perfect estimation) to \(1\)
      \end{itemize}
    \end{frame}

    \begin{frame}{Brier Score --- Simple Example}
      \begin{itemize}
        \item Suppose an estimator gave the following estimations:
        
        \begin{table}[]
          \begin{tabular}{ll}
            \(o_i\) & \(p_i\) \\
            \midrule
            0    & 0.1  \\
            1    & 0.9  \\
            1    & 0.8  \\
            0    & 0.3 
          \end{tabular}
        \end{table}
        \item Brier Score is \[\frac{1}{4}\left[{(0 - 0.1)}^2 + {(1 - 0.9)}^2 + {(1 - 0.8)}^2 + {(0 - 0.3)}^2\right] = 0.0375\]
      \end{itemize}
    \end{frame}  
    
    \begin{frame}{Brier Score --- Survival Analysis}
      \begin{itemize}
        \item Survival Curve predicts time dependent probabilities \(p_i(t)\)
        \item Brier score becomes time dependent:
        \[BS(t) = \frac{1}{N} \displaystyle \sum_{t=1}^{N} {(p_i(t) - o_i(t))}^2\]
      \end{itemize}
    \end{frame}

    \begin{frame}{Brier Score --- Survival Analysis II}
      \begin{itemize}
        \item Needs adaptation for censored data (basic idea: remove sample if censored before time of evaluation)
        \item Final version is Integrated Brier score~\cite{graf1999assessment}:
        \[IBS = \frac{1}{\max (t_i)} \int_{0}^{\max (t_i)} BS(t) dt\]
      \end{itemize}
    \end{frame}    

  \section{Our Application}

  \begin{frame}{Overview}
    \begin{itemize}
      \item Answer biological questions related to a prostate cancer patient's lifecycle
      \item Datasets
      \begin{itemize}
        \item PLCO
        \item Martini-Clinic
      \end{itemize}
      \item Models
      \begin{itemize}
        \item Cox Proportional Hazards
        \item DeepSurv
      \end{itemize}
    \end{itemize}

  \end{frame}
  
  \subsection{Example}

  \begin{frame}{Progression Free Survival After RP}
    \begin{itemize}
      \item How likely is a patient to remain progression free after radical prostatectomy (RP)?
      \begin{itemize}
        \item Did not have additional treatment
        \item Did not die of prostate cancer
      \end{itemize}
      \item Classic approach using Cox PH by~\cite{Kattan1999} to project 7 year progression free survival
      \item Task: Reevaluate on our datasets
    \end{itemize}
  \end{frame}

  \begin{frame}{Progression Free Survival After RP II}   
    \begin{figure}
      \includegraphics[width=.6\textwidth]{img/nomo-kattan-postop.png}
      \caption{Postoperative Nomogram for Prostate Cancer Recurrence by~\cite{Kattan1999}}
    \end{figure} 
  \end{frame}
    
  \begin{frame}{Martini-Clinic Data}
    \begin{itemize}
      \item \(\approx 16,000\) patients that received radical prostatectomy
      \item \(78\% \) censored
      \item median follow up of  \(3.2\) years (\(1170\) days)
    \end{itemize}
  \end{frame}
  
  \begin{frame}{KM Survival Plot I}
    \begin{figure}[]
      \centering
      \includegraphics[width=.95\textwidth]{img/mk_survival.png}
      \caption{Progression free survival of Martini-Clinic patients}
    \end{figure}
  \end{frame}

  \begin{frame}{ KM Survival Plot II}
    \begin{figure}[]
      \centering
      \includegraphics[width=.95\textwidth]{img/mk_survival_age.png}
      \caption{Progression free survival of Martini-Clinic patients stratified by age at RP}
    \end{figure}
  \end{frame}

  \begin{frame}{KM Survival Plot III}
    \begin{figure}[]
      \centering
      \includegraphics[width=.95\textwidth]{img/mk_survival_gleason.png}
      \caption{Progression free survival of Martini-Clinic patients stratified by gleason sum}
    \end{figure}
  \end{frame}    

  \begin{frame}{Data Preparation}
    \begin{itemize}
      \item Unbalanced
      \item Censored
      \item Compare with~\cite{Kattan1999} (hand picked) covariates
      \begin{itemize}
        \item Preoperative PSA value
        \item Gleason Sum
        \item Cancer Cells on Surgical Margin?
        \item Prostate Capsule Invaded?
        \item Seminal Vesicle Invaded?
        \item Lymph Nodes Invaded?
      \end{itemize}
      \item \alert{Linearity constrain}: Transform PSA value
    \end{itemize}
  \end{frame}   
  
  \begin{frame}{DeepSurv}
    \begin{itemize}
      \item MLP as input for Cox PH Model
    \end{itemize}
    \begin{figure}[]
      \centering
      \includegraphics[height=.5\textheight]{img/deep_surv_arch.jpg}
      \caption{DeepSurv Architecture (Cox-MLP)}
    \end{figure}
  \end{frame} 

  \begin{frame}{DeepSurv II}
    \begin{itemize}
      \item Simple Neural Network
      \begin{itemize}
        \item NN with 2 hidden layers and 64 nodes each
        \item \(40\% \) dropout
        \item batch normalization
      \end{itemize}
      \item Implemented in pycox\footnote{\href{https://github.com/havakv/pycox}{github.com/havakv/pycox}} package
      \item Only trained on patients that received event
    \end{itemize}
  \end{frame}
  
  \section{Results}

  \begin{frame}{DeepSurv on Martini-Clinic Data for Question 1}
    \begin{figure}
      \centering
      \animategraphics[height=.8\textheight]{12}{img/deepsurv-interactive-mk/frame-}{0}{203}
      \caption{Interactive Survival Plot using DeepSurv on Martini-Clinik data}
    \end{figure}
  \end{frame}    

  \begin{frame}{Performance}
    \begin{table}[]
      \begin{tabular}{l|ll|ll|}
        & \multicolumn{2}{c}{Concordance Index} & \multicolumn{2}{c}{Integrated Brier Score}\\
        & Martini-Clinic & PLCO & Martini-Clinic & PLCO \\
        \midrule
        CoxPH     & \(0.785 \pm 0.01\) & \(0.767 \pm 0.05\) (\(0.74\)) & * & * \\
        DeepSurv*  & \(0.781\) & \(0.736\) & \(0.147\) & \(0.092\) \\
      \end{tabular}
    \end{table}
    \vfill
    \begin{itemize}
      \scriptsize{\item[*] Evaluation pipeline not standardized yet}
    \end{itemize}
    

  \end{frame}

  \section{Repository}
  
  \begin{frame}{Overview}
    \begin{figure}[]
      \centering
      \includegraphics[width=.75\textwidth]{img/gitlab-ehr-pipeline.png}
      \caption{GitLab Screenshot of our EHR-pipeline}
    \end{figure}
  \end{frame}


  \begin{frame}{Features}
    \begin{itemize}
      \item Clinical data separated
      \item Data Preparation
      \item Evaluation
      \item \textcolor{gray}{Models}
      \item []
      \item Python Package
      \item CI with Unit Tests
      \item Python Code Style Check
      \item Releases with Versioning (Changelog)
    \end{itemize}
  \end{frame}  

  \section{Summary}

    \begin{frame}{Conclusion}
      \begin{itemize}
        \item Basic understanding of Survival Analysis
        \item Evaluation metrics are not intuitive / need close inspection
        \item Started implementing models
        \item No gain with first ``Vanilla'' NN, but comparable results
      \end{itemize}
    \end{frame}

    \begin{frame}{Outlook}
      \begin{itemize}
        \item Tune networks architectures  --- Optimize for what?
        \item How to improve NN architectures?
        \item Include additional features (e.g.\ family history)
        \begin{itemize}
          \item Age
          \item Family History
          \item \ldots 
          \item Latent Representation of whole patient Electronic Health Record
        \end{itemize} 
        \item Include in Package
        \begin{itemize}
          \item Models
          \item Hyperparameter Tuning
          \item Evaluation
          \item \ldots
        \end{itemize}
      \end{itemize}
    \end{frame}

  \begin{frame}[allowframebreaks]{References}
    \printbibliography[heading=none]
  \end{frame}

  \backupbegin{}

  \begin{frame}{Kaplan-Meier Curves II}
    \hfill
    \begin{columns}[T]
      \centering
      \column{0.3\textwidth} 
        \begin{exampleblock}{Advantages}
          \begin{itemize}
            \item[+] Average Survival Curve of a Population
            \item[+] Easy to Calculate
          \end{itemize}
        \end{exampleblock}
      \column{0.3\textwidth}
        \begin{alertblock}{Disadvantages}
          \begin{itemize}
            \item[-] No individual estimate
          \end{itemize}
        \end{alertblock}
    \end{columns}
  
    \pause{}
    \begin{itemize}
      \item What about the influence of covariates?
    \end{itemize}
  
  \end{frame}

  \begin{frame}{Calibration Plot}

    \begin{minipage}{.4\textwidth}
      \begin{itemize}
        \item Measures agreement between predicted probabilities and observed event rates 
        \item 45 degree line has perfect calibration
        \item model under line: overconfident, model below line: underconfident
        \item Qualitative Analysis
      \end{itemize}
    \end{minipage}\begin{minipage}{.59\textwidth}
      \begin{figure}[]
        \centering
        \includegraphics[width=\textwidth]{img/calibration-plot-sample.png}
        \caption{Calibration Plot from lifelines\footnotemark\ package}
      \end{figure}
    \end{minipage}
    \footnotetext{\href{lifelines.readthedocs.io}{lifelines.readthedocs.io}}
  \end{frame}

  \begin{frame}{Calibration Plot --- Remarks}
    \begin{figure}[]
      \centering
      \fbox{\includegraphics{img/calibration-quote-3.png}}
    \end{figure}      
    \hspace*\fill{\small---~\cite{Stephenson}}
    \begin{figure}[]
      \centering
      \fbox{\includegraphics{img/calibration-quote-1.png}}
    \end{figure}
    \hspace*\fill{\small---~\cite{Eastham2008}}
    \begin{figure}[]
      \centering
      \fbox{\includegraphics{img/calibration-quote-2.png}}
    \end{figure}
    \hspace*\fill{\small---~\cite{Gensheimer2019}}
  \end{frame}

  \backupend{}
  
  \end{document}
  
  
  % TODO reintroduce backup slides