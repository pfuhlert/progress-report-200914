% !TEX program = xelatex

\documentclass[aspectratio=1610, xcolor={dvipsnames}]{beamer}

\usepackage{amsmath,amsfonts,amsthm,bm}
\usepackage{animate}
\usepackage[backend=biber, style=apa]{biblatex}
\addbibresource{bib/library.bib}
\usepackage{booktabs}
\usepackage[english]{babel}
\usepackage{caption}
\usepackage{csquotes}
\usepackage{fontawesome}
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{romannum}
\usepackage{siunitx}
\usepackage{tikz}
\usepackage{tikzscale}
\usepackage{standalone}

% Experimental
% \useinnertheme{metropolis}
% \useoutertheme{metropolis}
% \usecolortheme{metropolis}
% \usefonttheme{professionalfonts}
\usepackage[mathrm=sym]{unicode-math}

% \defaultfontfeatures{ Scale = MatchUppercase }
% \setmainfont{FiraGO}[Scale = 1.0]
% \setsansfont{FiraGO}
% \setmonofont{Fira Mono}
\setmathfont{Fira Math}
% \setmathfont[range=up]{Fira Sans Regular}
% \setmathfont[range=it]{Fira Sans Italic}
% \setmathfont[range=bfup]{Fira Sans Bold}
% \setmathfont[range=bfit]{Fira Sans Bold Italic}

% ----------------------

\newcommand{\themename}{\textbf{\textsc{metropolis}}\xspace}

\usetikzlibrary{arrows.meta, fit, calc, matrix, positioning, chains, decorations.pathreplacing}
\usetheme{metropolis}
\title{Survival Analysis with Neural Networks on Electronic Health Records}
\date{14.09.2020}
\author{Patrick Fuhlert}
\institute{Institute of Medical Systems Biology}

\metroset{subsectionpage=progressbar, numbering=fraction, titleformat=smallcaps}

\makeatletter

\input{metropolis_extensions.tex}

\begin{document}

  \maketitle

  \begin{frame}{Content}
    \setbeamertemplate{section in toc}[sections numbered]
    \tableofcontents%[hideallsubsections]
  \end{frame}  

  \section{Introduction}
    
  \begin{frame}{Motivation}
    \begin{itemize}
      \item Direct Access to Martini-Klinik (UKE) data
      \begin{itemize}
        \item Prostate Cancer Clinic
        \item `With around  2400 prostate cancer operations per year [\ldots ] biggest Prostate Cancer Center [\ldots ] worldwide'\footnote{martini-klinik.de/en/for-patients/}
        \item Focus on (robot assisted) radical prostatectomy
      \end{itemize}
      \item We want to
      \begin{itemize}
        \item Find disease/medication/treatment \textbf{correlations}
        \item Predict risks and \textbf{future progression} for \textbf{clinical decision making}
        \item Patient Stratification
        \item Gain Results that are interpretable, avoid `\textbf{black box}' problem
      \end{itemize}
    \end{itemize}
  \end{frame}

    \begin{frame}{Electronic Health Records}
      \begin{description}
        \item[Incomplete data] Were there additional treatments outside of the current hospital? Is the patient absent because he is healthy or dead? 
        \item[Heterogeneity] Large differences in individual patient documentation as well as their granularity; tracking in inconsistent intervals.
        \item[Sparsity] The vast number of possible treatments, medications, diagnostic procedures and tests leads to high-dimensional and sparse EHR data, as a patient experiences only a small subset of all possible medical events.
        \item[Bias] EHRs for the same patient will most likely differ from doctor to doctor or hospital to hospital. Information that was stored in an EHR may also be filtered.
      \end{description}
    \end{frame}    

    \section{Prostate Cancer}

    \begin{frame}{Patient Lifecycle}
      \begin{figure}[]
        \centering
        \includegraphics[width=.75\textwidth]{img/pca-lifecycle.png}
        \caption{\scriptsize{Prostate Cancer Patient Lifecycle. AS=Active Surveillance, RT=Radiotherapy, RP=Radical Prostatectomy}}
      \end{figure}
      \begin{enumerate}
        \pause{}
        \item Is a biopsy necessary?
        \item What primary treatment should be applied?
        \item How long does a patient remain progression free?
        \item Is additional treatment after RP necessary?
      \end{enumerate}
    \end{frame}

    \begin{frame}{Questions}
      \begin{itemize}
        \item What primary treatment should be applied?
        \item How long does a patient remain progression free?
      \end{itemize}

      Closely related. Can be tried by prediction the progression free survival time for each treatment and choose the best outcome.
    \end{frame}

    \section{Survival Analysis}

    \begin{frame}{Overview}
      Survival analysis tries to find the expected duration of time until one or more events happen.
      \hspace*\fill{\small---~\cite{MillerJr2011}}
      \vspace{1em}
      
      \begin{description}[leftmargin=1em, labelwidth=2em]
        \item[Medical Survival] How long until patient dies?
        \item[Reliability Theory] How long until something breaks?
        \item[Economics] How long until a customer withdraws?
      \end{description}
    \end{frame}

    \begin{frame}{Survival Curve}
      \begin{figure}[]
        \centering
        \includegraphics[height=.5\textheight]{img/survival-plot-sample.png}
        \caption{Survival Curve of Life expectancy for (non-) smokers\footnote{Doll et al. 2004}}
      \end{figure}

      Try to find predicted curve \(\hat{S}(t)\) that approximates real survival curve \(S(t)\)
    \end{frame}
  
    \begin{frame}{Kaplan-Meier}

      \begin{minipage}{.25\textwidth}
        \begin{table}[]
          \begin{tabular}{rl}
            & \(t_i\) \\
            \midrule
            A & 1 \\
            B & 2 \\
            C & 3 \\
            D & 4 \\
            E & 5 \\
          \end{tabular} \\
        \end{table} 
        \textbf{Table 1:} Toy survival times               
      \end{minipage}\begin{minipage}{.74\textwidth}
        \pause{}
        \begin{figure}
          \scalebox{0.5}{\input{pgf/km.pgf}}
          \caption{Kaplan-Meier Estimate}
        \end{figure}
      \end{minipage}

      Kaplan-Meier approach: \(\hat{S}(t) = \displaystyle \prod_{t_i < t}(1 - \frac{d_i}{n_i})\) \\
      \vspace{1em}
      \scriptsize{where \(n_i\) is number of people at risk and \(d_i\) = number of death events at time \(t_i\)}
      
    \end{frame}
    
    \begin{frame}{Kaplan-Meier Curves}
      \hfill
      \begin{columns}[T]
        \centering
        \column{0.3\textwidth} 
          \begin{exampleblock}{Advantages}
            \begin{itemize}
              \item[+] Easy to Calculate
            \end{itemize}
          \end{exampleblock}
        \column{0.3\textwidth}
          \begin{alertblock}{Disadvantages}
            \begin{itemize}
              \item[-] Population Estimate
              \item[-] Independent of individual features 
            \end{itemize}
          \end{alertblock}
      \end{columns}
    
      %TODO add advantages/disadvantages
      
    
    \end{frame}

    \begin{frame}{Cox Proportional Hazard Model}
    \end{frame} 
  
    \begin{frame}{Survival Prediction with Neural Networks}
      \begin{figure}[]
        \fbox{\includestandalone[height=.8\textheight]{tikz/continuous-survival-nn}}
        \caption{Hybrid Model with Continuous Output}
      \end{figure}
    \end{frame}

    \begin{frame}{Survival Prediction with Neural Networks II}
      \begin{figure}[]
        \centering
        \fbox{\includestandalone[width=.4\textwidth]{tikz/discrete-nn}}
        \caption{Neural Network with Discrete Output}
      \end{figure}
      
      \begin{itemize}
        \item \(O_1, \dots, O_m\) predict survival at certain times \(t_1, \dots, t_m\)
      \end{itemize}
    \end{frame}   

    \subsection{Performance Measures}

    \begin{frame}{Concordance Index}
      \begin{itemize}
        \item Generalization of the area under the ROC curve for censored data
        \item Fraction of correct order of predictions \(\hat{t}_i\) vs outcome \(t_i\): 
        \[\text{c-index = } \displaystyle \frac{\sum_{i, j} 1_{t_j < t_i} \cdot 1_{\hat{t}_j < \hat{t}_i}}{\sum_{i, j} 1_{t_j < t_i}}\]
        \item Leave out a pair if the concordance is unknown due to censoring
        \item Perfect score is \(1\), random prediction is \(\approx 0.5\)
      \end{itemize}
    \end{frame}

    \begin{frame}{Concordance Index --- Example}

      \begin{minipage}{0.49\textwidth}
        \begin{table}[]
          \begin{tabular}{rll}
            & \(t_i\) & \(\hat{t}_i\) \\
            \midrule
            A & 1 & 1  \\
            B & 2 & 2  \\
            C & 3 & 3  \\
            D & 4 & 4  \\
            E & 5 & 5 \\
          \end{tabular} \\
          \pause{}
        \end{table}
        \centering
        c-index = \(1\)
      \end{minipage}
      \pause{}
      \begin{minipage}{0.49\textwidth}
        \begin{table}[]
        \begin{tabular}{rll}
          & \(t_i\) & \(\hat{t}_i\) \\
          \midrule
          A & 1 & 2  \\
          B & 2 & 3  \\
          C & 3 & 5  \\
          D & 4 & 8  \\
          E & 5 & 14 \\
        \end{tabular} \\
      \end{table}
      \centering
      c-index = \(1\)      
      \end{minipage}
      \pause{}
      \begin{itemize}
        \item[]
        \item[]
        \item[] Only ordinality is considered, not the actual predicted values!
      \end{itemize}
    \end{frame}    

    \begin{frame}{Concordance Index --- Example II}
      Was event observed? \(\delta_i = 1\) 
      \begin{table}[]
          \centering
          \begin{tabular}{rlll}
            & \(t_i\) & \(\hat{t}_i\) & \(\delta_i\) \\
            \midrule
            A & 1 & 1 & 1 \\
            B & 2 & 2 & 1 \\
            C & 3 & 3 & 0 \\
            D & 4 & \alert{5} & 1 \\
            E & 5 & \alert{4} & 1 \\
          \end{tabular} \\
      \end{table}
      \vspace{1cm}
      \[\text{c-index = }\frac{|\{(B, A), (C, A), (C, B), (D, A), (D, B), (E, A), (E, B)\}|}{10 - |\{(D, C), (E, C)\}|} = 0.875\]      
    \end{frame}      

    \begin{frame}{Brier Score}
      \begin{itemize}
        \item Consider \(p_i = \hat{S}(x_i)\) as the predicted survival of an individuum at a specific time \(t\)
        \item[] \[BS = \frac{1}{N} \displaystyle \sum_{t=1}^{N} {(p_i - o_i)}^2\]
        \item Mean squared error between prediction \(p_i\) and outcome \(o_i\)
        \item Loss Function
        \item Ranges from \(0\) (perfect estimation) to \(1\)
      \end{itemize}
    \end{frame}

    \begin{frame}{Brier Score --- Simple Example}
      \begin{itemize}
        \item Suppose an estimator gave the following estimations:
        
        \begin{table}[]
          \begin{tabular}{ll}
            \(o_i\) & \(p_i\) \\
            \midrule
            0    & 0.1  \\
            1    & 0.9  \\
            1    & 0.8  \\
            0    & 0.3 
          \end{tabular}
        \end{table}
        \item Brier Score is \[\frac{1}{4}\left[{(0 - 0.1)}^2 + {(1 - 0.9)}^2 + {(1 - 0.8)}^2 + {(0 - 0.3)}^2\right] = 0.0375\]
      \end{itemize}
    \end{frame}  
    
    \begin{frame}{Brier Score --- Survival Analysis}
      \begin{itemize}
        \item Brier score becomes time dependent:
        \[BS(t) = \frac{1}{N} \displaystyle \sum_{t=1}^{N} {(p_i(t) - o_i(t))}^2\]
        \item Needs adaptation for censored data (basic idea: remove sample if censored before time of evaluation)
        \item Final version is Integrated Brier score\footnote{Graf et al. 1999}:
        \[IBS = \frac{1}{\max (t_i)} \int_{0}^{\max (t_i)} BS(t) dt\]
      \end{itemize}
    \end{frame}    

    \begin{frame}{Calibration Plot}

      \begin{minipage}{.4\textwidth}
        \begin{itemize}
          \item Measures agreement between predicted probabilities and observed event rates 
          \item 45 degree line has perfect calibration
          \item model under line: overconfident, model below line: underconfident
          \item Qualitative Analysis
        \end{itemize}
      \end{minipage}\begin{minipage}{.59\textwidth}
        \begin{figure}[]
          \centering
          \includegraphics[width=\textwidth]{img/calibration-plot-sample.png}
          \caption{Calibration Plot from lifelines\footnotemark\ package}
        \end{figure}
      \end{minipage}
      \footnotetext{\href{lifelines.readthedocs.io}{lifelines.readthedocs.io}}
    \end{frame}
    
    \begin{frame}{Calibration Plot --- Remarks}
      \begin{figure}[]
        \centering
        \fbox{\includegraphics{img/calibration-quote-3.png}}
      \end{figure}      
      \hspace*\fill{\small---~\cite{Stephenson}}
      \begin{figure}[]
        \centering
        \fbox{\includegraphics{img/calibration-quote-1.png}}
      \end{figure}
      \hspace*\fill{\small---~\cite{Eastham2008}}
      \begin{figure}[]
        \centering
        \fbox{\includegraphics{img/calibration-quote-2.png}}
      \end{figure}
      \hspace*\fill{\small---~\cite{Gensheimer2019}}
    \end{frame}

    \subsection{Application to our data}

    \begin{frame}{Data Preparation}
      \begin{itemize}
        \item Unbalanced
        \item Censored

      \end{itemize}
    \end{frame}   
    
  \section{Results}
  \subsection{Current Status}

  \begin{frame}{Data Sources}
    \begin{itemize}
      \item PLCO
      \item Martini-Clinic
    \end{itemize}

    Input data right now is same as for nomograms -> e.g.\ no time dependency

    Optimize dataset:
    balance data,
    transform to linear features (e.g. PSA value)
  \end{frame}

  \begin{frame}{Martini-Clinic}
  \end{frame}    

  \begin{frame}{DeepSurv}
    \begin{itemize}
      \item MLP as input for Cox Proportional Hazard Model
    \end{itemize}
    \begin{figure}[]
      \includegraphics[height=.5\textheight]{img/deep_surv_arch.jpg}
      \caption{DeepSurv Architecture (Cox-MLP)}
    \end{figure}
  \end{frame} 

  \begin{frame}{DeepSurv on PLCO data}

    \begin{itemize}
      \item Show Architecture
      \item Show image survival
    \end{itemize}

    % \animategraphics[height=.8\textheight]{12}{img/deepsurv-interactive/frame-}{0}{204}
  \end{frame}

  \begin{frame}{Repository}
    \begin{figure}[]
      \centering
      \includegraphics[width=.75\textwidth]{img/gitlab-ehr-pipeline.png}
      \caption{GitLab Screenshot of our EHR-pipeline}
    \end{figure}
  \end{frame}

  \begin{frame}{Repository --- Features}
    \begin{itemize}
      \item Clinical data separated
      \item CI with Unit Tests
      \item python code style checker
      \item Versioning (Changelog)
      \item Integrate models into package
      \item Integrate evaluation metrics into package
    \end{itemize}
  \end{frame}  

  \begin{frame}{Results}
    On PLCO data
    \begin{itemize}
      \item LR
      \item CoxPH
      \item DeepSurv
    \end{itemize}
  
    On MK data
    \begin{itemize}
      \item LR
      \item CoxPH
      \item DeepSurv
    \end{itemize}

  \end{frame}

  \subsection{Summary}

    \begin{frame}{Conclusion}
      \begin{itemize}
        \item First architectures from literature implemented
        \item On our 2 datasets
        \item Add more datasets
        \item Tune networks  architectures as is
        \item Look into Proper data representation
        \item Evaluation metrics are not intuitive / need close inspection
      \end{itemize}
    \end{frame}

  \begin{frame}[allowframebreaks]{References}
    \printbibliography[heading=none]
  \end{frame}

  \backupbegin{}

  \begin{frame}{Backup 1}
    \begin{itemize}
      \item K-Fold CV
    \end{itemize}
  \end{frame}

  \backupend{}

\end{document}